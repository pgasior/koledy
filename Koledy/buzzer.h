/*
 * buzzer.h
 *
 * Created: 2014-12-03 21:31:38
 *  Author: Piotrek
 */

#ifndef BUZZER_H_
#define BUZZER_H_

#include <stdint.h>
/*
 #define C	118
 #define Cis	111
 #define D	105
 #define Des	99
 #define E	93
 #define F	88
 #define Fis	83
 #define G	78
 #define Gis	74
 #define A	70
 #define As	66
 #define H	62
 #define C6	58
 */
//#define FLOPPY 1
extern uint8_t floppy;
#define PRESKALER 8
#if PRESKALER==1
#define C2		61151
#define Cis2	57719
#define D2		54480
#define Dis2	51426
#define E2		48536
#define F2		45812
#define Fis2	43242
#define G2		40815
#define Gis2	38523
#define A2		36362
#define Ais2	34321
#define H2		32395
#define C3		30577
#define Cis3	28861
#define D3		27241
#define Dis3	25712
#define E3		24269
#define F3		22907
#define Fis3	21620
#define G3		20407
#define Gis3	19262
#define A3		18180
#define Ais3	17160
#define H3		16197
#define C4		15287
#define Cis4	14430
#define D4		13620
#define Dis4	12855
#define E4		12133
#define F4		11452
#define Fis4	10810
#define G4		10203
#define Gis4	9630
#define A4		9089
#define Ais4	8579
#define H4		8098
#define C5		7643
#define Cis5	7214
#define D5		6809
#define Dis5	6427
#define E5		6066
#define F5		5725
#define Fis5	5404
#define G5		5101
#define Gis5	4814
#define A5		4544
#define Ais5	4289
#define H5		4048
#define C6		3821
#define Cis6	3606
#define D6		3404
#define Dis6	3213
#define E6		3032
#define F6		2862
#define Fis6	2701
#define G6		2550
#define Gis6	2406
#define A6		2271
#define Ais6	2144
#define H6		2023
#define C7		1910
#define Cis7	1802
#define D7		1701
#define Dis7	1606
#define E7		1515
#define F7		1430
#define Fis7	1350
#define G7		1274
#define Gis7	1202
#define A7		1135
#define Ais7	1071
#define H7		1011
#define C8		954
#define Cis8	900
#define D8		850
#define Dis8	802
#define E8		757
#define F8		714
#define Fis8	674
#define G8		636
#define Gis8	600
#define A8		567
#define Ais8	535
#define H8		505
#endif

#if PRESKALER==8
#define C0		30580
#define Cis0  	28867
#define D0		27246
#define Dis0  	25705
#define E0		24270
#define F0		22903
#define Fis0  	21625
#define G0		20407
#define Gis0  	19259
#define A0		18180
#define Ais0  	17157
#define H0		16195
#define C1		15289
#define Cis1  	14429
#define D1		13619
#define Dis1  	12855
#define E1		12134
#define F1		11453
#define Fis1  	10809
#define G1		10203
#define Gis1  	9631
#define A1		9089
#define Ais1	8579
#define H1		8097
#define C2		7643
#define Cis2	7214
#define D2		6809
#define Dis2	6427
#define E2		6066
#define F2		5725
#define Fis2	5404
#define G2		5101
#define Gis2	4814
#define A2		4544
#define Ais2	4289
#define H2		4048
#define C3		3821
#define Cis3	3606
#define D3		3404
#define Dis3	3213
#define E3		3032
#define F3		2862
#define Fis3	2701
#define G3		2550
#define Gis3	2406
#define A3		2271
#define Ais3	2144
#define H3		2023
#define C4		1910
#define Cis4	1802
#define D4		1701
#define Dis4	1606
#define E4		1515
#define F4		1430
#define Fis4	1350
#define G4		1274
#define Gis4	1202
#define A4		1135
#define Ais4	1071
#define H4		1011
#define C5		954
#define Cis5	900
#define D5		850
#define Dis5	802
#define E5		757
#define F5		714
#define Fis5	674
#define G5		636
#define Gis5	600
#define A5		567
#define Ais5	535
#define H5		505
#define C6		476
#define Cis6	449
#define D6		424
#define Dis6	400
#define E6		378
#define F6		356
#define Fis6	336
#define G6		317
#define Gis6	299
#define A6		283
#define Ais6	267
#define H6		252
#define C7		237
#define Cis7	224
#define D7		211
#define Dis7	199
#define E7		188
#define F7		177
#define Fis7	167
#define G7		158
#define Gis7	149
#define A7		141
#define Ais7	133
#define H7		125
#define C8		118
#define Cis8	111
#define D8		105
#define Dis8	99
#define E8		93
#define F8		88
#define Fis8	83
#define G8		78
#define Gis8	74
#define A8		70
#define Ais8	66
#define H8		62
#endif

#define OSEMKA 1
#define CWIERC 2
#define POL 4
#define CALA 8
#define CWIERCK 3
#define POLK 6


extern uint16_t cwierc;
extern uint16_t cala;
extern uint16_t pol;
extern uint16_t osemka;
extern uint16_t cwiercK;
extern uint16_t calaK;
extern uint16_t polK;
//extern uint16_t osemkaK;

void buzzer_init();
void ustaw_tepo(uint16_t tempo);
void graj_nute(const uint16_t nuta, const uint16_t czas);
void graj_pauze(const uint16_t czas);
void ustaw_piosenke(uint8_t nr);

#if PRESKALER==1
#define START_TIMER1 TCCR1B |= (1<<CS10)
#define STOP_TIMER1 TCCR1B &= ~(1<<CS10)
#endif
#if PRESKALER==8
#define START_TIMER1 TCCR1B |= (1<<CS11)
#define STOP_TIMER1 TCCR1B &= ~(1<<CS11)
#endif

#define START_TIMER0 TCCR0B |= ((1<<CS01)  | (1<<CS00))
#define STOP_TIMER0 TCCR0B &= ~((1<<CS01)  | (1<<CS00))

#define DIR_PIN_HIGH PORTC |= (1<<PC4)
#define DIR_PIN_LOW PORTC &= ~(1<<PC4)

#define STEP_PIN_HIGH PORTC |= (1<<PC5)
#define STEP_PIN_LOW PORTC &= ~(1<<PC5)

//#if FLOPPY==1
#define MAX_POSITION 158
void reset_floppy();
void zmien_instrument();
void stop();
extern volatile int8_t pitch;
//#endif

#endif /* BUZZER_H_ */
