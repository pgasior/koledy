/*
 * lcd_menu.c
 *
 *  Created on: 12 gru 2014
 *      Author: Piotrek
 */

#include <avr/io.h>
#include "lcd_menu.h"
#include "../LCD/lcd44780.h"
#include "../PS2kb/ps2kb.h"

uint8_t down_lock;
uint8_t up_lock;
uint8_t enter_lock;
struct menuentry *currentry;

void menu_up(void)
{
	if (currentry->prev == NULL)
		return;
	currentry = currentry->prev;
	display_menu();
}
void menu_down(void)
{
	if (currentry->next == NULL)
		return;
	currentry = currentry->next;
	display_menu();

}
void menu_enter(void)
{
	if (currentry->submenu != 0)
	{
		currentry = currentry->submenu;
		display_menu();
	}
	if (currentry->func != NULL)
		currentry->func(currentry->arg);

}
void display_menu(void)
{
	lcd_cls();
	lcd_locate(0, 0);
	lcd_char('>');
	lcd_str_P(currentry->text);
	if (currentry->next != NULL)
	{
		lcd_locate(1, 0);
		lcd_char(' ');
		lcd_str_P(currentry->next->text);
	}
}

void scan_keys(void)
{
	if (!up_lock && key == UP)
	{
		up_lock = 1;
		menu_up();
	}
	else if (up_lock && key != UP)
		up_lock = 0;

	if (!down_lock && key == DOWN)
	{
		down_lock = 1;
		menu_down();
	}
	else if (down_lock && key != DOWN)
		down_lock = 0;

	if (!enter_lock && key == ENTER)
	{
		enter_lock = 1;
		menu_enter();
	}
	else if (enter_lock && key != ENTER)
		enter_lock = 0;
}

void menu_set(struct menuentry* start)
{
	currentry = start;
}
