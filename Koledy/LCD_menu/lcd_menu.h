/*
 * lcd-menu.h
 *
 *  Created on: 12 gru 2014
 *      Author: Piotrek
 */

#ifndef LCD_MENU_LCD_MENU_H_
#define LCD_MENU_LCD_MENU_H_

#include <avr/io.h>
#ifndef NULL
#define NULL   ((void *) 0)
#endif
typedef void (*menu_function)(uint8_t);
struct menuentry
{
	const char *text;
	struct menuentry *submenu;
	struct menuentry *prev;
	struct menuentry *next;
	menu_function func;
	int arg;
};

#define MAKE_MENU_ENTRY(entry,d_text,d_func,d_next,d_prev,d_submenu,d_arg) \
	entry.text = PSTR(d_text); \
	entry.func = d_func; \
	entry.next = d_next; \
	entry.prev = d_prev; \
	entry.submenu = d_submenu; \
	entry.arg = d_arg

void menu_up(void);
void menu_down(void);
void menu_enter(void);
void display_menu(void);
void scan_keys(void);
void menu_set(struct menuentry* start);

#endif /* LCD_MENU_LCD_MENU_H_ */
