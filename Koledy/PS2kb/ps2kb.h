/*
 * ps2kb.h
 *
 *  Created on: 12 gru 2014
 *      Author: Piotrek
 */

#ifndef PS2KB_H_
#define PS2KB_H_

#include <avr/io.h>

#define USE_PS2_WAITKEY		0

#define PS2_DATA_DDR    DDRD
#define PS2_DATA_PORT    PORTD
#define PS2_DATA_PIN    PIND
#define PS2_DATA_BIT    4
#define PS2_CLK_DDR        DDRD
#define PS2_CLK_PORT    PORTD
#define PS2_CLK_PIN        PIND
#define PS2_CLK_BIT        2

#define UP 17
#define DOWN 18
#define LEFT 19
#define RIGHT 20
#define PAUSE 22
#define ENTER 13

extern volatile uint8_t key;

uint8_t ps2_waitkey(void);
void ps2_init(void);


#endif /* PS2KB_H_ */
