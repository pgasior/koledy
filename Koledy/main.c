/*
 * koledy.c
 *
 * Created: 1011-11-01 11:10:11
 *  Author: Piotrek
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "buzzer.h"
#include <util/delay.h>
#include "LCD/lcd44780.h"
#include "LCD_menu/lcd_menu.h"
#include "PS2kb/ps2kb.h"
#include <avr/pgmspace.h>
/*
 #define START_TIMER1 TCCR1B |= (1<<CS11)
 #define START_TIMER0 TCCR0B |= ((1<<CS01)  | (1<<CS00))
 #define STOP_TIMER0 TCCR0B &= ~((1<<CS01)  | (1<<CS00))
 */

struct menuentry mBogSieRodzi;
struct menuentry mPrzybiezeli;
struct menuentry mLulajze;
struct menuentry mCichaNoc;

uint8_t zmiana_lock = 0;
uint8_t stop_lock = 0;
uint8_t pitch_up_lock = 0;
uint8_t pitch_down_lock = 0;

/*void fBogSieRodzi(void)
 {
 STOP_TIMER0;
 STOP_TIMER1;
 ustaw_piosenke(0);
 reset_floppy();
 START_TIMER0;
 }

 void fPrzybiezeli(void)
 {
 STOP_TIMER0;
 STOP_TIMER1;
 ustaw_piosenke(2);
 reset_floppy();
 START_TIMER0;
 }

 void fLulajze(void)
 {
 STOP_TIMER0;
 STOP_TIMER1;
 ustaw_piosenke(1);
 reset_floppy();
 START_TIMER0;
 }*/

void fMenu(uint8_t arg)
{
	STOP_TIMER0;
	STOP_TIMER1;
#if FLOPPY == 0
	TCCR1A &= ~(1 << COM1A0);
	PORTB &= ~(1 << PB1);
#endif
	ustaw_piosenke(arg);
#if FLOPPY==1
	reset_floppy();
#endif
	_delay_ms(500);
#if FLOPPY==0
	TCCR1A |= (1 << COM1A0);
#endif
	START_TIMER0;
}

int main(void)
{
	MAKE_MENU_ENTRY(mBogSieRodzi, "Bog sie rodzi", fMenu, &mPrzybiezeli, NULL,
			NULL, 0);
	MAKE_MENU_ENTRY(mPrzybiezeli, "Przybiezeli do Betlejem", fMenu, &mLulajze,
			&mBogSieRodzi, NULL, 2);
	MAKE_MENU_ENTRY(mLulajze, "Lulajze Jezuniu", fMenu, &mCichaNoc,
			&mPrzybiezeli, NULL, 1);
	MAKE_MENU_ENTRY(mCichaNoc, "Cicha Noc", fMenu, NULL, &mLulajze, NULL, 3);
	//_delay_ms(6000);
	buzzer_init();
	lcd_init();
	ps2_init();
	menu_set(&mBogSieRodzi);

	ustaw_tepo(120);
	sei();
//#if FLOPPY==1
	reset_floppy();
//#endif
	//_delay_ms(2000);
	//START_TIMER0;
	display_menu();
	while (1)
	{
		scan_keys(); //menu

		if (!zmiana_lock && key == 1)
		{
			zmiana_lock = 1;
			zmien_instrument();
			lcd_cls();
			if (floppy == 1)
				lcd_str_P(PSTR("Stacje dyskietek"));
			else
				lcd_str_P(PSTR("Buzzer"));
			_delay_ms(500);
			display_menu();
		}
		else if (zmiana_lock && key != 1)
			zmiana_lock = 0;

		if (!stop_lock && key == 27) //escape
		{
			stop_lock = 1;
			stop();
		}
		else if (stop_lock && key != 27)
			stop_lock = 0;

		if (!pitch_up_lock && key == RIGHT)
		{
			pitch_up_lock = 1;
			pitch++;
		}
		else if (pitch_up_lock && key != RIGHT)
			pitch_up_lock = 0;

		if (!pitch_down_lock && key == LEFT)
		{
			pitch_down_lock = 1;
			pitch--;
		}
		else if (pitch_down_lock && key != LEFT)
			pitch_down_lock = 0;

	}
}
