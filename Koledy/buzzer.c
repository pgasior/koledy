/*
 * buzzer.c
 *
 * Created: 2014-12-03 21:31:26
 *  Author: Piotrek
 */
#include "buzzer.h"
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "koledy.h"

uint16_t cwierc;
uint16_t cala;
uint16_t pol;
uint16_t osemka;
uint16_t cwiercK;
uint16_t calaK;
uint16_t polK;
volatile uint16_t licznik_timer0;
volatile uint16_t czas_timer0;
volatile uint8_t czekaj = 0;
volatile uint8_t piosenka = 0;

volatile uint16_t trwa = 0;
volatile uint8_t nuta = 0;
volatile uint16_t dzwiek;
uint8_t floppy = 1;
volatile int8_t pitch = 0;

//#if FLOPPY==1
volatile uint8_t currentPosition = 0;
volatile uint8_t currentState = 0;
//#endif
void zmien_instrument()
{
	if (floppy == 1)
	{
		TCCR1A &= ~(1 << COM1A0);
		PORTB &= ~(1 << PB1);
		pitch=3;
	}
	else
		pitch = 0;
		//TCCR1A |= (1 << COM1A0);
	floppy = !floppy;
}

void ustaw_piosenke(uint8_t nr)
{
	piosenka = nr;
	trwa = 0;
	dzwiek = 0;
	nuta = 0;
}

//#if FLOPPY==1
ISR (TIMER1_COMPA_vect)
{
	if (floppy == 0)
	{
		//currentState=!currentState;
		return;
	}


	if (currentPosition >= MAX_POSITION)
	{
		DIR_PIN_HIGH;
		currentState = 1;
	}
	else if (currentPosition <= 0)
	{
		DIR_PIN_LOW;
		currentState = 0;
	}

	if (currentState == 1)
		currentPosition--;
	else
		currentPosition++;

	PORTC ^= (1 << PC5);
}
void reset_floppy()
{
	uint8_t i;
	DIR_PIN_HIGH;
	for (i = 0; i < 80; i++)
	{
		STEP_PIN_HIGH;
		STEP_PIN_LOW;
		_delay_ms(5);
	}
	currentPosition = 0;
	currentState = 0;
	DIR_PIN_LOW;
}
//#endif

ISR (TIMER0_COMPA_vect)
{

	if (!trwa)
	{
		/*volatile const uint16_t * aktualny_dzwiek_ptr = (const uint16_t *)pgm_read_word(&(piosenka_nuty_ptr[piosenka]));
		 volatile uint16_t aktualny_dzwiek = pgm_read_word(&(aktualny_dzwiek_ptr[nuta]));
		 dzwiek = aktualny_dzwiek;*/
		dzwiek = pgm_read_word(&(piosenka_nuty_ptr[piosenka][nuta]));
		if (dzwiek) //piosenka_nuty_ptr[piosenka][nuta])
		{
//#if FLOPPY==0
			if (floppy == 0)
				TCCR1A |= (1 << COM1A0);
			if (floppy == 1)
			{
				TCCR1A &= ~(1 << COM1A0);
				PORTB &= ~(1 << PB1);
			}

//#endif
			TCNT1 = 0;
			if(pitch>=0)
				OCR1A = dzwiek>>pitch; //piosenka_nuty_ptr[piosenka][nuta];
			else
				OCR1A = dzwiek<<-pitch;
			START_TIMER1;
		}
		/*
		 #if FLOPPY==1
		 else
		 STOP_TIMER1;
		 #else
		 */
		else
		{
			STOP_TIMER1;
			if (floppy == 0)
			{
				TCNT1 = 0;
				TCCR1A &= ~(1 << COM1A0);
				PORTB &= ~(1 << PB1);
				//TCCR1A |= (1<<COM1A0);
			}

		}
//#endif
	}
	trwa++;
	if (trwa >= piosenka_czasy_ptr[piosenka][nuta]) //piosenka_czasy_ptr[piosenka][nuta])
	{
		trwa = 0;
		STOP_TIMER1;
		nuta++;
		//aktualny_dzwiek = pgm_read_word(&(piosenka_nuty_ptr[piosenka][nuta]));
	}
	if (nuta >= piosenka_dlugosc[piosenka])
	{
		nuta = 0;
		//aktualny_dzwiek = pgm_read_word(&(piosenka_nuty_ptr[piosenka][nuta]));
		//piosenka=(piosenka+1)%LICZBA_PIOSENEK;
		STOP_TIMER1;
		STOP_TIMER0;
	}

}

void buzzer_init()
{
//#if FLOPPY==0
	DDRB |= (1 << PB1);
	PORTB &= ~(1 << PB1);
	//TCCR1A |= (1 << COM1A0); //Toggle ORC1A
	TCCR1B |= (1 << WGM12); //CTC

	TCCR0A |= (1 << WGM01);

	TIMSK0 |= (1 << OCIE0A);
	OCR0A = 124;
//#else
	DDRC |= ((1 << PC5) | (1 << PC4)); //PC5 step, PC4 DIR
	PORTC &= ~((1 << PC5) | (1 << PC4));

	//TCCR1B |= (1 << WGM12);
	TIMSK1 |= (1 << OCIE1A);

	//TCCR0A |= (1 << WGM01);
	//TIMSK0 |= (1 << OCIE0A);
	//OCR0A = 124;
//#endif
}
void stop()
{
	STOP_TIMER0;
	STOP_TIMER1;
	if (floppy == 0)
	{

		TCNT1 = 0;
		TCCR1A &= ~(1 << COM1A0);
		_delay_ms(20);
		PORTB &= ~(1 << PB1);
		//TCCR1A |= (1<<COM1A0);
	}

}
void ustaw_tepo(uint16_t tepo)
{
	cwierc = 60000 / tepo;
	cala = cwierc * 4;
	pol = cwierc * 2;
	osemka = cwierc / 2;
	cwiercK = cwierc + osemka;
	calaK = cala + pol;
	polK = pol + cwierc;
	uint8_t i, j;
	for (i = 0; i < LICZBA_PIOSENEK; i++)
	{
		for (j = 0; j < piosenka_dlugosc[i]; j++)
		{
			piosenka_czasy_ptr[i][j] *= osemka;
		}
	}
}

