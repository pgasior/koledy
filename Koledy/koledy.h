/*
 * koledy.h
 *
 *  Created on: 6 gru 2014
 *      Author: Piotrek
 */

#ifndef KOLEDY_H_
#define KOLEDY_H_
#include <stdint.h>
#include "buzzer.h"
#include <avr/pgmspace.h>
#define LICZBA_PIOSENEK 4

const uint16_t bog_nuty[] PROGMEM=
{
C3,C4,C4,H3,A3,
A3,A3,A3,G3,F3,
E3,G3,C4,G3,F3,E3,
E3,D3,0,

C3,C4,C4,H3,A3,
A3,A3,A3,G3,F3,
E3,G3,C4,G3,F3,E3,
E3,D3,0,

F3,D3,E3,F3,
G3,G3,E3,G3,
F3,D3,E3,F3,
G3,G3,E3,G3,
F3,D3,E3,F3,

G3,G3,E3,G3,
F3,D3,E3,F3,
G3,H2,D3,C3,
0
};

uint16_t bog_czasy[] =
{
CWIERC,CWIERC,CWIERC,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,CWIERC,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,OSEMKA,OSEMKA,OSEMKA,OSEMKA,
CWIERC,CWIERC,CWIERC,

OSEMKA,OSEMKA,CWIERC,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,CWIERC,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,OSEMKA,OSEMKA,OSEMKA,OSEMKA,
CWIERC,CWIERC,CWIERC,

CWIERCK,OSEMKA,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,CWIERC,CWIERC,
CWIERCK,OSEMKA,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,CWIERC,CWIERC,
CWIERCK,OSEMKA,OSEMKA,OSEMKA,

OSEMKA,OSEMKA,CWIERC,CWIERC,
CWIERCK,OSEMKA,OSEMKA,OSEMKA,
OSEMKA,OSEMKA,CWIERC,CWIERC,
CALA
};


const uint16_t lulajze_nuty[] PROGMEM =
{
E3, E3, E3,
G3, F3, E3, F3,
D3, D3, F3,
A3, G3, 0,
E3, E3, E3,
G3, F3, E3, F3,
D3, G3, F3,
F3, E3, 0,
0};

uint16_t lulajze_czasy[] =
{
CWIERC, CWIERC, CWIERC,
CWIERC, OSEMKA, OSEMKA, CWIERC,
CWIERC, CWIERC, CWIERC,
CWIERC, CWIERC, CWIERC,
CWIERC, CWIERC, CWIERC,
CWIERC, OSEMKA, OSEMKA, CWIERC,
CWIERC, CWIERCK, OSEMKA,
CWIERC, CWIERC, CWIERC,
CALA};


const uint16_t przybiezeli_nuty[] PROGMEM = //cis fis
{
D3, Cis3, D3, E3,
Fis3, E3, Fis3, G3,
A3, H3,
A3,
D3, Cis3, D3, E3,

Fis3, E3, Fis3, G3,
A3, H3,
A3,
D4, A3, A3,
H3, A3, G3, Fis3,
G3, G3, G3,

A3, G3, Fis3, E3,
Fis3, G3,
A3,
Fis3, E3,
D3,
0};

uint16_t przybiezeli_czasy[] =
{
OSEMKA, OSEMKA, OSEMKA, OSEMKA,
OSEMKA, OSEMKA, OSEMKA, OSEMKA,
CWIERC, CWIERC,
POL,
OSEMKA, OSEMKA, OSEMKA, OSEMKA,

OSEMKA, OSEMKA, OSEMKA, OSEMKA,
CWIERC, CWIERC,
POL,
CWIERC, OSEMKA, OSEMKA,
OSEMKA, OSEMKA, OSEMKA, OSEMKA,
CWIERC, OSEMKA, OSEMKA,

OSEMKA, OSEMKA, OSEMKA, OSEMKA,
CWIERC, CWIERC,
POL,
CWIERC, CWIERC,
POL,
CALA
};

//cis fis gis
const uint16_t cicha_noc_nuty[] PROGMEM =
{
		E3,Fis3,E3,
		Cis3,
		E3,Fis3,E3,
		Cis3,
		H3,H3,
		Gis3,Gis3,
		A3,A3,
		E3,
		Fis3,Fis3,

		A3,Gis3,Fis3,
		E3,Fis3,E3,
		Cis3,Cis3,
		Fis3,Fis3,
		A3,Gis3,Fis3,
		E3,Fis3,E3,
		Cis3,Cis3,
		H3,H3,

		D4,H3,Gis3,
		A3,
		Cis4,0,
		A3,E3,Cis3,
		E3,D3,H2,
		A2,
		0
};

uint16_t cicha_noc_czasy[] =
{
		CWIERCK,OSEMKA,CWIERC,
		POLK,
		CWIERCK,OSEMKA,CWIERC,
		POLK,
		POL,CWIERC,
		POL,CWIERC,
		POL,CWIERC,
		POLK,
		POL,CWIERC,

		CWIERCK,OSEMKA,CWIERC,
		CWIERCK,OSEMKA,CWIERC,
		POL,CWIERC,
		POL,CWIERC,
		CWIERCK,OSEMKA,CWIERC,
		CWIERCK,OSEMKA,CWIERC,
		POL,CWIERC,
		POL,CWIERC,

		CWIERCK,OSEMKA,CWIERC,
		POL,
		POL,CWIERC,
		CWIERC,CWIERC,CWIERC,
		CWIERCK,OSEMKA,CWIERC,
		POLK,
		CALA
};


const uint16_t* piosenka_nuty_ptr[LICZBA_PIOSENEK] =
{  bog_nuty,lulajze_nuty, przybiezeli_nuty, cicha_noc_nuty };

uint16_t* piosenka_czasy_ptr[LICZBA_PIOSENEK] =
{bog_czasy, lulajze_czasy,  przybiezeli_czasy, cicha_noc_czasy };

uint8_t piosenka_dlugosc[] =
{ sizeof(bog_czasy)/sizeof(uint16_t), sizeof(lulajze_nuty)/sizeof(uint16_t), sizeof(przybiezeli_nuty)/sizeof(uint16_t), sizeof(cicha_noc_czasy)/sizeof(uint16_t) };

#endif /* KOLEDY_H_ */
